<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Producto $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="producto-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form
            ->field($model, 'precio')
            ->input('number')
    ?>
    
    
    <?= $form
            ->field($model, 'codigo_fabricante') 
            ->dropDownList($model->fabricantes) // desplegable
            //->listBox($model->fabricantes())  // caja lista
    ?>

    <div class="form-group">
        <?= Html::submitButton('Aceptar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
