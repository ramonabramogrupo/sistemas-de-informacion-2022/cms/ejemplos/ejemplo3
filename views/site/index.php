<?php
/* @var $this yii\web\View */

$this->title = 'Gestion de tienda';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">Gestion de tienda</h1>

        <p class="lead">Administracion de los productos y los fabricantes</p>

    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-6">
                <h2 class="text-center">Productos</h2>
                <div>
                    <?php
                    echo \yii\helpers\Html::img(
                            "@web/imgs/productos.png",
                            [
                                'class' => 'img-thumbnail d-block mx-auto'
                            ]
                    );
                    ?>
                </div>


            </div>
            <div class="col-lg-6">
                <h2 class="text-center">Fabricantes</h2>
                <div>
                    <?php
                    echo \yii\helpers\Html::img(
                            "@web/imgs/fabricantes.jpg",
                            [
                                'class' => 'img-thumbnail d-block mx-auto'
                            ]
                    );
                    ?>
                </div>
            </div>
        </div>

    </div>
</div>
