<?php

use app\models\Fabricante;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */
$this->title = 'Fabricantes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fabricante-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <div>
        <?= Html::a('Modo Lista', ['index'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('+', ['create'], ['class' => 'btn btn-success']) ?>
    </div>
    <br>


    <?=
    \yii\widgets\ListView::widget([
        "dataProvider" => $dataProvider,
        "itemView" => "_ver",
        "itemOptions" => [
            'class' => 'col-lg-3 ml-auto mr-auto bg-light p-3 mb-5',
        ],
        "options" => [
            'class' => 'row',
        ],
        'layout' => "{items}{pager}",
    ])
    ?>


</div>
