<div class="row">
    <div class="col-lg-12">
        <h2>Codigo: <?= $model->codigo ?><br></h2>
        <div class="text-white bg-primary rounded p-2">Nombre:</div>
        <div class="p-1"><?= $model->nombre ?></div>
        <?=
        yii\helpers\Html::a(
                //"ver", // texto del boton
                '<i class="fal fa-eye"></i>', // imagen del boton
                [
                    'view', // accion
                    'codigo' => $model->codigo // parametro
                ],
                [
                    "class" => "btn btn-success"
                ]
        ) // boton para ver el fabricante
        ?>

        <?=
        yii\helpers\Html::a(
                //"editar", // texto del boton
                '<i class="fad fa-pencil"></i>', //imagen del boton
                [
                    'update', // accion
                    'codigo' => $model->codigo // parametro
                ],
                [
                    "class" => "btn btn-success"
                ]
        ) // boton para modificar el fabricante
        ?>
        <?=
        yii\helpers\Html::a(
                //"eliminar", // texto del boton
                '<i class="fad fa-trash-alt"></i>', //imagen del boton
                [
                    'delete', // accion
                    'codigo' => $model->codigo // parametro
                ],
                [
                    "class" => "btn btn-danger",
                    'data' => [
                        'confirm' => '¿Estas seguro que deseas eliminar el fabricante?',
                        'method' => 'post',
                    ],
                ]
        ) // boton para eliminar el fabricante
        ?>
        <br class="float-none">
    </div>
</div>


