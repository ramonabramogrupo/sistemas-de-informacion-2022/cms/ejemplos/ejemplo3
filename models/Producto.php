<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "producto".
 *
 * @property int $codigo
 * @property string $nombre
 * @property float $precio
 * @property int $codigo_fabricante
 *
 * @property Fabricante $codigoFabricante
 */
class Producto extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'producto';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'precio', 'codigo_fabricante'], 'required'],
            [['precio'], 'number'],
            [['codigo_fabricante'], 'integer'],
            [['nombre'], 'string', 'max' => 100],
            [['codigo_fabricante'], 'exist', 'skipOnError' => true, 'targetClass' => Fabricante::class, 'targetAttribute' => ['codigo_fabricante' => 'codigo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'nombre' => 'Nombre',
            'precio' => 'Precio',
            'codigo_fabricante' => 'Codigo Fabricante',
        ];
    }

    /**
     * Gets query for [[CodigoFabricante]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoFabricante()
    {
        return $this->hasOne(Fabricante::class, ['codigo' => 'codigo_fabricante']);
    }
    
    public function fabricantes(){
        $listado=Fabricante::find()->all();
        return \yii\helpers\ArrayHelper::map(
                $listado,
                "codigo",
                "nombre"
            );
    }
}
